import Axios from "axios";
import {url} from "./url";

export const getAllTeams = () => {
  return Axios.get(url + "/getAllTeams").then(res => {
    return res.data;
  });
};

export const postTeams = x => {
  return Axios.post(url + "/addTeam", x)
    .then(res => res.data)
    .catch(err => err);
};

export const putTeams = (id, x) => {
  return Axios.put(url + "/updateTeam/" + id, x)
    .then(res => res.data)
    .catch(err => err);
};
