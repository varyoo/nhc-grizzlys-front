import Axios from "axios";
import Crypt from "../../utils/utilis.encrypt";
import {url} from "./url";

export function postLogin(data) {
  let password = "";
  Crypt.CryptContent(JSON.stringify(data.password), res => (password = res));
  return Axios.post(url + "/login", {
    username: data.username,
    password: password,
  }).then(res => {
    return res.data.token;
  });
}
