import Axios from "axios";
import {url} from "./url";

export const getAllTrainings = () => {
  return Axios.get(url + "/getAllTrainings").then(res => {
    return res.data;
  });
};
export const getAllTrainingsByAdmin = () => {
  return Axios.get(url + "/getAllTrainings").then(res => {
    return res.data;
  });
};
export const getByPeriodTraining = period => {
  return Axios.get(url + "/getTrainingPeriod/" + period).then(res => {
    return res.data;
  });
};

export const postTraining = data => {
  return Axios.post(url + "/addTraining", data)
    .then(res => {
      return res;
    })
    .catch(err => console.log(err));
};

export const putTraining = (id, data) => {
  return Axios.put(url + "/updateTraining/" + id, data)
    .then(res => {
      return res;
    })
    .catch(err => console.log(err));
};

export const deleteTraining = id => {
  return Axios.delete(url + "/deleteTraining/" + id)
    .then(res => {
      return res;
    })
    .catch(err => console.log(err));
};
