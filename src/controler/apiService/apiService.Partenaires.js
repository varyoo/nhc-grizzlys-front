import Axios from "axios";
import {url} from "./url";

export const getAllPartenaires = async () => {
  try {
    const res = await Axios.get(url + "/getAllPartenaires");
    return res.data;
  } catch (err) {
    return err;
  }
};

export const deletePartenaires = async id => {
  try {
    const res = await Axios.delete(url + "/deletePartenaire/" + id);
    return res.data;
  } catch (err) {
    return err;
  }
};

export const updatePartenaires = async (id, data) => {
  try {
    const res = await Axios.put(url + "/updatePartenaire/" + id, data);
    return res.data;
  } catch (err) {
    return err;
  }
};

export const addPartenaires = async data => {
  try {
    const res = await Axios.post(url + "/addPartenaire", data);
    return res.data;
  } catch (err) {
    return err;
  }
};
