import Axios from "axios";
import {url} from "./url";

export const get20Match = x => {
  return Axios.get(url + "/get20Match/" + x)
    .then(res => res.data)
    .catch(err => err);
};

export const addMatch = data => {
  return Axios.post(url + "/addMatch", data)
    .then(res => data.res)
    .catch(err => console.log(err));
};
