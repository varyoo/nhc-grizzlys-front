export const defaultStateTraining = {
  cloakroom_training: "",
  day_training: "",
  end_hour_training: "",
  start_hour_training: "",
  period: "",
  teams: [],
};

export const ControllerStateUpdateTraining = (data, returnData) => {
  let tempA = [data.teams.map(value => value.id_team_teams)];
  const trainingUp = {
    cloakroom_training: data.cloakroom_training,
    day_training: data.day_training,
    end_hour_training: data.end_hour_training,
    start_hour_training: data.start_hour_training,
    period: data.period,
    teams: tempA[0],
  };

  returnData(trainingUp);
};
