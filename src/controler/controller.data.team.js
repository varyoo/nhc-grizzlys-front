export const ControllerStateUpdateTeam = (data, returnData) => {
  const team = {
    id_team_teams: data.id_team_teams,
    name_team: data.name_team,
    presentation_team: data.presentation_team,
    supervisor_1: data.supervisor_1,
    function_supervisor1: data.function_supervisor1,
    supervisor_2: data.supervisor_2,
    function_supervisor2: data.function_supervisor2,
    supervisor_3: data.supervisor_3,
    function_supervisor3: data.function_supervisor3,
  };
  const picture = {
    id_picture: data.picture.id_picture,
    name_picture: data.picture.name_picture,
    alt_picture: data.picture.alt_picture,
  };
  const pic_Supervisor = {
    pic_supervisor1: data.pic_supervisor1,
    pic_supervisor2: data.pic_supervisor2,
    pic_supervisor3: data.pic_supervisor3,
  };
  returnData(team, picture, pic_Supervisor);
};

export const defaultStateTeam = {
  id_team_teams: "",
  name_team: "",
  presentation_team: "",
  supervisor_1: "",
  function_supervisor1: "",
  supervisor_2: "",
  function_supervisor2: "",
  supervisor_3: "",
  function_supervisor3: "",
};
export const defaultStatePicture = {
  id_picture: "",
  name_picture: "",
  alt_picture: "",
};
export const defaultStatePic_Supervisor = {
  pic_supervisor1: {
    name_picture: "supervisor1-" + Date.now(),
    alt_picture: "supervisor1",
  },
  pic_supervisor2: {
    name_picture: "supervisor2-" + Date.now(),
    alt_picture: "supervisor2",
  },
  pic_supervisor3: {
    name_picture: "supervisor3-" + Date.now(),
    alt_picture: "supervisor3",
  },
};
