import React from "react";
import {Route, Redirect} from "react-router-dom";

const PrivateRoute = ({component: Component, ...rest}) => {
  return (
    <Route
      {...rest}
      render={props =>
        //localStorage.getItem("token")
        true ? <Component {...props} /> : <Redirect to="/connexion" />
      }
    />
  );
};

export default PrivateRoute;
