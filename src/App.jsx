import React from "react";
import {Route} from "react-router-dom";
import Admin from "./components/admin/admin.index";
import Users from "./components/users/users.index";
import Login from "./components/auth/login.index";
import PrivateRoute from "./controler/controler.privateRoute";

const App = () => {
  return (
    <>
      <Route exact path="/" component={Users} />
      <Route path="/connexion" component={Login} />
      <PrivateRoute path="/admin" component={Admin} />
    </>
  );
};

export default App;
