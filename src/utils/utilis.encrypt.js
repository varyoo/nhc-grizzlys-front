const CryptoJS = require("crypto-js");

class Crypt {
  static CryptContent(req, returnContenteCrypted) {
    const crypted = CryptoJS.AES.encrypt(req, "Ajouter ici la cle de cryptage");
    returnContenteCrypted(crypted.toString());
  }
}

export default Crypt;
