import {url} from "controler/apiService/url";
import React from "react";
import {Form, Button} from "react-bootstrap";
import TimePicker from "react-times";
import "react-times/css/material/default.css";

export const InputsText = ({label, type, defaultValue, action}) => {
  return (
    <Form.Group controlId="formBasicText">
      <Form.Label>{label}</Form.Label>
      <Form.Control
        type="text"
        placeholder={defaultValue}
        onChange={e => action(type, e.target.value)}
      />
    </Form.Group>
  );
};
export const InputsFile = ({label, defaultValue, action}) => {
  return (
    <Form.Group controlId="formBasicText">
      <Form.Label>{label}</Form.Label>
      <Form.File
        placeholder={defaultValue}
        onChange={e => action(e.target.files)}
        accept="image/jpeg"
      />
    </Form.Group>
  );
};
export const CheckBox = ({label, value, id, action, isChecked}) => {
  return (
    <Form.Group controlId="formBasicCheckbox" id={id}>
      <Form.Check
        type="checkbox"
        label={label}
        checked={isChecked}
        onChange={() => action(value)}
      />
    </Form.Group>
  );
};
export const RadioBox = ({label, value, id, action, name, isChecked}) => {
  return (
    <Form.Group controlId="formBasicCheckbox" onChange={() => action(value)}>
      <Form.Check type="radio" name={name} label={label} id={id} checked={isChecked} />
    </Form.Group>
  );
};
export const ButtonForms = ({label, action}) => {
  return (
    <Button variant="primary" onClick={e => action(e)}>
      {label}
    </Button>
  );
};

export const InputsTime = ({action, label, defaultValue, type}) => {
  return (
    <Form.Group controlId="fromBasicHour">
      <Form.Label>{label}</Form.Label>
      <TimePicker
        timeMode="24"
        time={defaultValue ? defaultValue : "15:00"}
        onTimeChange={e => action(type, e)}
      />
    </Form.Group>
  );
};

export const InputsImage = ({src, alt}) => {
  return <img src={url + "/files/" + src + ".jpg"} alt={alt} />;
};
