import React, {useState, useEffect} from "react";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBNavbarToggler,
  MDBCollapse,
} from "mdbreact";
import {BrowserRouter as Router} from "react-router-dom";
import {ListComponents} from "./user.list.components";

const NavBar = goTo => {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {}, [isOpen]);

  const toggleCollapse = () => {
    setIsOpen(!isOpen);
  };

  const handleClick = id => {
    goTo.handleClick(id);
  };

  return (
    <header>
      <MDBNavbar dark expand="md" fixed="top" className="header">
        <MDBNavbarBrand>
          <strong className="white-text">
            <img src={require("assets/images/logo.png")} alt="" />
          </strong>
        </MDBNavbarBrand>
        <MDBNavbarToggler onClick={() => toggleCollapse()} />
        <MDBCollapse
          id="navbarCollapse3"
          isOpen={isOpen}
          navbar
          className="d-flex justify-content-center"
        >
          <MDBNavbarNav center className="header-nav">
            {ListComponents.map(item => {
              return (
                <MDBNavItem key={item.id} className="header-nav-item">
                  <MDBNavLink to="/" onClick={() => handleClick(item.id)}>
                    {item.name}
                  </MDBNavLink>
                </MDBNavItem>
              );
            })}
          </MDBNavbarNav>
        </MDBCollapse>
      </MDBNavbar>
    </header>
  );
};
export default NavBar;
