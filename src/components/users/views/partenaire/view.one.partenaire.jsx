import React from "react";
import {InputsImage} from "utils/inputs/inputs";

const OnePartenaire = ({partenaire, picture}) => {
  return (
    partenaire && (
      <div style={{border: "1px solid black", backgroundColor: "white"}}>
        <h1>{partenaire.name_partner}</h1>
        <div>
          {picture && (
            <InputsImage src={picture.name_picture} alt={picture.alt_picture} />
          )}
        </div>
        <div>
          <p>{partenaire.description_partner}</p>
          <p>{partenaire.city_partner}</p>
          <p> {partenaire.street_partner}</p>
          <p>
            <a target="blank" href={partenaire.website_partner}>
              {partenaire.website_partner}
            </a>
          </p>
          <p> {partenaire.phone_partner}</p>
          <p> {partenaire.mail_partner}</p>
          <p> {partenaire.zip_partner}</p>
        </div>
      </div>
    )
  );
};

export default OnePartenaire;
