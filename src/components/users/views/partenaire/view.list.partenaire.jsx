import React, {useEffect, useState} from "react";
import {getAllPartenaires} from "controler/apiService/apiService.Partenaires";

const ListPartenaire = ({action}) => {
  const [allPartenaire, setAllPartenaire] = useState([
    {
      name_partner: "",
      description_partner: "",
      city_partner: "",
      zip_partner: 0,
      street_partner: "",
      phone_partner: 0,
      mail_partner: "",
      website_partner: "",
      picture: {
        id_picture: 0,
        name_picture: "",
        alt_picture: "",
      },
    },
  ]);

  useEffect(() => {
    getAllPartenaires().then(res => {
      setAllPartenaire(res);
      action(res[0]);
    });
  }, []);

  return (
    allPartenaire && (
      <ul>
        {allPartenaire.map((value, key) => (
          <li
            style={{border: "1px solid black", backgroundColor: "white"}}
            key={key}
            onClick={() => action(value)}
          >
            {value.name_partner}
          </li>
        ))}
      </ul>
    )
  );
};

export default ListPartenaire;
