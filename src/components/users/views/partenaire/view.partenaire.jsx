import React, {useState} from "react";
import "styles/users.styles/users.styles.partenaire.scss";
import {ButtonForms} from "utils/inputs/inputs";
import ListPartenaire from "./view.list.partenaire";
import OnePartenaire from "./view.one.partenaire";

const Partenaire = () => {
  const [viewOnePartenaire, setViewOnePartenaire] = useState({});
  const handleChange = value => {
    setViewOnePartenaire(value);
  };
  return (
    <div className="main-partenaire">
      <h1>Nos PARTENAIRES</h1>
      <ListPartenaire action={value => handleChange(value)} />
      <OnePartenaire partenaire={viewOnePartenaire} picture={viewOnePartenaire.picture} />
      <ButtonForms
        label="Devenir Partenaire"
        action={() => alert("Devenir Partenaire")}
      />
    </div>
  );
};

export default Partenaire;
