import React from "react";
import "styles/users.styles/users.styles.news.one.scss";
import {Card} from "react-bootstrap";
import {MDBIcon} from "mdbreact";
import img from "assets/images/img_test.jpg";

const OneNews = props => {
  return (
    <div className="main-news-single">
      <button className="main-news-single-button" onClick={() => props.backToAllNews()}>
        <MDBIcon icon="angle-left" />
        Retour
      </button>
      <div className="main-news-single-body">
        <div className="main-news-single-body-header">
          <h3>{props.news.title_news}</h3>
          <span>{props.news.date_news}</span>
        </div>
        <div className="main-news-single-body-content row">
          <div className="main-news-single-body-content-picture col-lg-6">
            <Card.Img src={img} alt={props.news.picture.alt_picture} />
          </div>
          <div className="main-news-single-body-content-text col-lg-6">
            <p>{props.news.article_news}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OneNews;
