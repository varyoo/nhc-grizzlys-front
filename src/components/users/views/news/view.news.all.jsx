import React, {useEffect, useState} from "react";
import "styles/users.styles/users.styles.news.scss";
import {Card} from "react-bootstrap";
import img from "assets/images/img_test.jpg";
import {get5news} from "controler/apiService/apiService.News";

const AllNews = ({viewNews}) => {
  const [news, setNews] = useState([]);
  const [indexNews, setIndexNews] = useState(0);
  const [isLast, setIsLast] = useState(true);

  useEffect(() => {
    get5news(indexNews).then(e => {
      setNews(e.news);
      setIsLast(e.newsLeft);
    });
  }, [indexNews]);

  return (
    <div className="main-news">
      <div className="cards">
        {news &&
          news.map((value, key) => {
            return (
              <div
                key={key}
                onClick={() => viewNews(value)}
                className={("column", key === 0 ? "last" : "other")}
              >
                <Card className="view overlay zoom">
                  <div className="pseudo-bg"></div>
                  <Card.Img
                    variant="top"
                    src={img}
                    className="img-fluid"
                    alt="smaple image"
                  />
                  <div className="card-text">
                    <div className="card-text-title">{value.title_news}</div>
                    <div className="card-text-date">{value.date_news}</div>
                  </div>
                  <div className="mask flex-center pseudo-bg-text">
                    <p className="white-text">Lire l'article</p>
                  </div>
                </Card>
              </div>
            );
          })}
      </div>
      {indexNews !== 0 && (
        <button onClick={() => setIndexNews(indexNews - 5)}>Precedent</button>
      )}
      {isLast && <button onClick={() => setIndexNews(indexNews + 5)}>Suivant</button>}
    </div>
  );
};

export default AllNews;
