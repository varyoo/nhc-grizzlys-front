import React from "react";
import "styles/users.styles/users.styles.entrainement.scss";
import Trainings from "./view.entrainement.training";

const Days = ({day, training}) => {
  return (
    training.length !== 0 && (
      <div>
        <h1>{day}</h1>
        {training.length !== 0 &&
          training.map((value, key) => {
            return <Trainings key={key} training={value} />;
          })}
      </div>
    )
  );
};

export default Days;
