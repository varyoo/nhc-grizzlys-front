import React from "react";
import "styles/users.styles/users.styles.entrainement.scss";
import {parsingHour} from "utils/utilis.parsingHour";

const Trainings = ({training}) => {
  let startHourParsed = parsingHour(training.start_hour_training);
  let endHourParsed = parsingHour(training.end_hour_training);

  return (
    <div>
      <span>
        {startHourParsed} à {endHourParsed}
      </span>
      <br />
      <span>{training.cloakroom_training}</span>
      {training.teams.length !== 0 &&
        training.teams.map((value, key) => {
          return <p key={key}>{value.name_team}</p>;
        })}
    </div>
  );
};

export default Trainings;
