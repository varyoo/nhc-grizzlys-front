import React, {useEffect, useState} from "react";
import {getByPeriodTraining} from "controler/apiService/apiService.Entrainements";
import "styles/users.styles/users.styles.entrainement.scss";
import {ButtonForms} from "utils/inputs/inputs";
import Days from "./view.entrainement.days";

const Entrainements = () => {
  const [period, setPeriod] = useState("scolaire");
  const [viewDayTraining, setViewDayTraining] = useState([]);

  useEffect(() => {
    getByPeriodTraining(period).then(res => {
      setViewDayTraining(res);
    });
  }, [period]);

  return (
    <div className="main-entrainement">
      {viewDayTraining.length !== 0 &&
        viewDayTraining.days.map((value, key) => {
          return <Days key={key} day={value.day} training={value.trainings} />;
        })}
      <ButtonForms label="Vacances" action={() => setPeriod("vacances")} />
      <ButtonForms label="Période Scolaire" action={() => setPeriod("scolaire")} />
    </div>
  );
};

export default Entrainements;
