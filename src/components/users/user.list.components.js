import React from "react";
import Accueil from "./views/accueil/view.accueil";
import Club from "./views/club/view.club";
import Contact from "./views/contact/view.contact";
import Equipe from "./views/equipes/view.equipes";
import Partenaire from "./views/partenaire/view.partenaire";
import News from "./views/news/view.news.index";
import Entrainements from "./views/entrainement/view.entrainement";

export const ListComponents = [
  {
    id: 0,
    name: "Accueil",
    Component: <Accueil />,
  },
  {
    id: 1,
    name: "Club",
    Component: <Club />,
  },
  {
    id: 2,
    name: "Contact",
    Component: <Contact />,
  },
  {
    id: 3,
    name: "Entrainements",
    Component: <Entrainements />,
  },
  {
    id: 4,
    name: "Equipes",
    Component: <Equipe />,
  },
  {
    id: 5,
    name: "News",
    Component: <News />,
  },
  {
    id: 6,
    name: "Partenaires",
    Component: <Partenaire />,
  },
];
