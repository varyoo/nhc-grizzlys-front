import React from "react";
import {Link} from "react-router-dom";
import "styles/admin.styles/admin.styles.scss";
const ListRouteComponents = () => {
  return (
    <ul className="navbar-nav mr-auto left-menu">
      <li>
        <Link to={"/admin/equipe"} className="nav-link">
          Équipes
        </Link>
      </li>
      <li>
        <Link to={"/admin/entrainements"} className="nav-link">
          Entrainements
        </Link>
      </li>
      <li>
        <Link to={"/admin/matchs"} className="nav-link">
          Matchs
        </Link>
      </li>
      <li>
        <Link to={"/admin/news"} className="nav-link">
          News
        </Link>
      </li>
      <li>
        <Link to={"/admin/association"} className="nav-link">
          Association
        </Link>
      </li>
      <li>
        <Link to={"/admin/partenaires"} className="nav-link">
          Partenaires
        </Link>
      </li>
    </ul>
  );
};

export default ListRouteComponents;
