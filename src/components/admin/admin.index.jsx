import React from "react";
import "styles/admin.styles/admin.styles.scss";
import {Switch, Route} from "react-router-dom";
import News from "./views/news/news.index";
import AdminHome from "components/admin/views/admin.home";
import Equipe from "./views/equipes/equipe.index";
import Partenaire from "./views/partenaire/partenaire.index";
import Entrainement from "./views/entrainement/index.entrainement";
import Match from "./views/match/match.index";

const Admin = () => {
  return (
    <div className="admin">
      <h1 className="admin-title">Administration</h1>
      <Switch>
        <Route exact path="/admin" component={AdminHome} />
        <Route path="/admin/equipe" component={Equipe} />
        <Route path="/admin/news" component={News} />
        <Route path="/admin/partenaires" component={Partenaire} />
        <Route path="/admin/entrainements" component={Entrainement} />
        <Route path="/admin/matchs" component={Match} />
      </Switch>
    </div>
  );
};

export default Admin;
