import React from "react";
import "styles/admin.styles/admin.styles.scss";
import {Link} from "react-router-dom";

const AdminHome = () => {
  return (
    <>
      <ul className="navbar-nav mr-auto admin-menu">
        <li>
          <Link to={"/admin/equipe"} className="nav-link admin-menu-link">
            <img src={require("assets/images/group-of-people.svg")} alt="" />
            Gérer les équipes
          </Link>
        </li>
        <li>
          <Link to={"/admin/entrainements"} className="nav-link admin-menu-link">
            <img src={require("assets/images/strong.svg")} alt="" />
            Gérer les entrainements
          </Link>
        </li>
        <li>
          <Link to={"/admin/matchs"} className="nav-link admin-menu-link">
            <img src={require("assets/images/player.svg")} alt="" />
            Gérer les matchs
          </Link>
        </li>
        <li>
          <Link to={"/admin/news"} className="nav-link admin-menu-link">
            <img src={require("assets/images/newspaper.svg")} alt="" />
            Gérer les news
          </Link>
        </li>
        <li>
          <Link to={"/admin/association"} className="nav-link admin-menu-link">
            <img src={require("assets/images/interaction.svg")} alt="" />
            Gérer l'association
          </Link>
        </li>
        <li>
          <Link to={"/admin/partenaires"} className="nav-link admin-menu-link">
            <img src={require("assets/images/briefcase.svg")} alt="" />
            Gérer les partenaires
          </Link>
        </li>
      </ul>
    </>
  );
};

export default AdminHome;
