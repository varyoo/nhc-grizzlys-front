import ListRouteComponents from "components/admin/ListRouteComponents";
import React, {useState} from "react";
import TeamForm from "./team.form";
import TeamList from "./team.list";

const Equipe = () => {
  const [editMode, setEditMode] = useState(false);
  const [team, setTeam] = useState();

  const addTeam = () => {
    setEditMode(true);
  };

  const updateTeam = teamToUpdate => {
    setTeam(teamToUpdate);
    setEditMode(true);
  };
  const handleSubmit = e => {
    e.preventDefault();
    setEditMode(false);
  };

  return (
    <div>
      <h1>Team Index</h1>
      <div>
        <ListRouteComponents />
      </div>
      <div>
        {editMode ? (
          <TeamForm updateTeam={team} action={e => handleSubmit(e)} />
        ) : (
          <TeamList
            addTeam={() => addTeam()}
            updateTeam={teamToUpdate => updateTeam(teamToUpdate)}
          />
        )}
      </div>
    </div>
  );
};

export default Equipe;
