import {getAllTeams} from "controler/apiService/apiService.Teams";
import React, {useEffect, useState} from "react";
import {ButtonForms} from "utils/inputs/inputs";

const TeamList = ({addTeam, updateTeam}) => {
  const [allTeam, setAllTeam] = useState([]);

  useEffect(() => {
    getAllTeams().then(res => {
      setAllTeam(res);
    });
  }, []);

  return (
    <div>
      <div>
        <ButtonForms label="Ajouter une Team" action={e => addTeam()} />
      </div>
      <div>
        <h1>Liste des Team</h1>
        {allTeam &&
          allTeam.map((value, key) => {
            return (
              <div key={key}>
                <span>{value.name_team}</span>
                <ButtonForms
                  label={"editer " + value.name_team}
                  action={e => updateTeam(value)}
                />
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default TeamList;
