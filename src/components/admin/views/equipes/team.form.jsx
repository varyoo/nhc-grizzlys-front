import React, {useState, useEffect} from "react";
import {
  ControllerStateUpdateTeam,
  defaultStateTeam,
  defaultStatePicture,
  defaultStatePic_Supervisor,
} from "controler/controller.data.team";
import {postTeams, putTeams} from "controler/apiService/apiService.Teams";
import {Form} from "react-bootstrap";
import {ButtonForms, InputsText, InputsFile} from "utils/inputs/inputs";
import Wysiwyg from "utils/inputs/wysiwyg";

const TeamForm = ({updateTeam}) => {
  const [team, setTeam] = useState(defaultStateTeam);
  const [picture, setPicture] = useState(defaultStatePicture);
  const [pictureSupervisor, setPictureSupervisor] = useState(defaultStatePic_Supervisor);
  const [file, setFile] = useState({});

  useEffect(() => {
    if (updateTeam) {
      ControllerStateUpdateTeam(updateTeam, (team, picture, pic_Supervisor) => {
        setTeam(team);
        setPicture(picture);
        setPictureSupervisor(pic_Supervisor);
      });
    }
  }, []);

  const handleChangeTeam = (type, value) => {
    setTeam({...team, [type]: value});
  };
  const handleChangePicture = (type, value) => {
    setPicture({...picture, [type]: value});
  };
  const handleChangeFile = (type, e) => {
    setFile({...file, [type]: e[0]});
  };

  const handleSubmit = e => {
    e.preventDefault();
    const forms = new FormData();
    let sub = {...team, picture: picture};
    forms.append("team", JSON.stringify(sub));
    forms.append("files", file.fileTeam);
    if (file.fileSup1) {
      forms.append("fileSup1", file.fileSup1);
    }
    if (file.fileSup2) {
      forms.append("fileSup2", file.fileSup2);
    }
    if (file.fileSup3) {
      forms.append("fileSup3", file.fileSup3);
    }
    if (updateTeam.id_team_teams) {
      putTeams(updateTeam.id_team_teams, forms);
    } else {
      postTeams(forms);
    }
  };

  return (
    <Form style={{width: "50%", margin: "0 auto"}}>
      <InputsText
        label="Titre"
        defaultValue={team.name_team}
        type="name_team"
        action={(type, e) => handleChangeTeam(type, e)}
      />
      <Wysiwyg
        label="Contenu"
        defaultValue={team.presentation_team}
        type="presentation_team"
        action={(type, html) => handleChangeTeam(type, html)}
      />
      <InputsFile label="Images" action={e => handleChangeFile("fileTeam", e)} />
      <InputsText
        label="Nom images"
        defaultValue={picture.name_picture}
        type="name_picture"
        action={(type, e) => handleChangePicture(type, e)}
      />
      <InputsText
        label="Description de l'images"
        type="alt_picture"
        defaultValue={picture.alt_picture}
        action={(type, e) => handleChangePicture(type, e)}
      />
      <div>
        <div>
          <p>Encadrant 1</p>
          <InputsText
            type="supervisor_1"
            defaultValue={team.supervisor_1}
            action={(type, e) => handleChangeTeam(type, e)}
          />
          <p>Fonciton</p>
          <InputsText
            type="function_supervisor1"
            defaultValue={team.function_supervisor1}
            action={(type, e) => handleChangeTeam(type, e)}
          />
          <InputsFile label="Images" action={e => handleChangeFile("fileSup1", e)} />
        </div>
        <div>
          <p>Encadrant 2</p>
          <InputsText
            type="supervisor_2"
            defaultValue={team.supervisor_2}
            action={(type, e) => handleChangeTeam(type, e)}
          />
          <p>Fonciton</p>
          <InputsText
            type="function_supervisor2"
            defaultValue={team.function_supervisor2}
            action={(type, e) => handleChangeTeam(type, e)}
          />
          <InputsFile label="Images" action={e => handleChangeFile("fileSup2", e)} />
        </div>
        <div>
          <p>Encadrant 3</p>
          <InputsText
            type="supervisor_3"
            defaultValue={team.supervisor_3}
            action={(type, e) => handleChangeTeam(type, e)}
          />
          <p>Fonciton</p>
          <InputsText
            type="function_supervisor3"
            defaultValue={team.function_supervisor3}
            action={(type, e) => handleChangeTeam(type, e)}
          />
          <InputsFile label="Images" action={e => handleChangeFile("fileSup3", e)} />
        </div>
      </div>
      <div>
        <ButtonForms label="Enregistrer" action={e => handleSubmit(e)} />
        <ButtonForms label="Annuler" action={e => handleSubmit(e)} />
      </div>
    </Form>
  );
};

export default TeamForm;
