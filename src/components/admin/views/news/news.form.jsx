import React, {useState} from "react";
import {Form} from "react-bootstrap";
import {getCurrentDate} from "utils/utilis.getCurentDate";
import {ButtonForms, InputsText, InputsFile} from "utils/inputs/inputs";
import {postNews, updateNews} from "controler/apiService/apiService.News";
import Wysiwyg from "utils/inputs/wysiwyg";

const NewsForm = ({updateOneNews}) => {
  const [news, setNews] = useState({
    title_news: updateOneNews.title_news || "",
    date_news: updateOneNews.date_news || getCurrentDate(),
    article_news: updateOneNews.article_news || "",
  });
  const [picture, setPicture] = useState({
    name_picture: "",
    alt_picture: "",
  });

  const [file, setFile] = useState([]);

  const handleChangeNews = (type, value) => {
    setNews({...news, [type]: value});
  };
  const handleChangePicture = (type, value) => {
    setPicture({...picture, [type]: value});
  };

  const handleChangeFile = file => {
    setFile(file);
  };

  const handleSubmit = e => {
    e.preventDefault();
    let sub = {...news, picture: picture};
    const forms = new FormData();
    forms.append("news", JSON.stringify(sub));
    forms.append("files", file[0]);
    if (updateOneNews.id_news) {
      updateNews(updateOneNews.id_news, forms);
    } else {
      postNews(forms);
    }
  };

  return (
    <Form style={{width: "100%", margin: "0 auto"}}>
      <InputsText
        label="Titre"
        defaultValue={news.title_news}
        type="title_news"
        action={(type, e) => handleChangeNews(type, e)}
      />
      <Wysiwyg
        label="Contenu"
        type="article_news"
        action={(type, html) => handleChangeNews(type, html)}
      />
      <InputsFile label="Images" action={e => handleChangeFile(e)} />
      <InputsText
        label="Nom images"
        defaultValue={picture.name_picture}
        type="name_picture"
        action={(type, e) => handleChangePicture(type, e)}
      />
      <InputsText
        label="Discription de l'images"
        type="alt_picture"
        defaultValue={picture.alt_picture}
        action={(type, e) => handleChangePicture(type, e)}
      />
      <ButtonForms label="Ajoutez l'article" action={e => handleSubmit(e)} />
    </Form>
  );
};

export default NewsForm;
