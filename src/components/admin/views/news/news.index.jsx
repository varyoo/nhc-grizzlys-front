import React, {useState} from "react";
import ListRouteComponents from "../../ListRouteComponents";
import NewsList from "./news.list";
import "styles/admin.styles/admin.news.styles.scss";
import NewsForm from "./news.form";
import {deleteNews} from "controler/apiService/apiService.News";

const News = () => {
  const [editMode, setEditMode] = useState(false);
  const [news, setNews] = useState({});

  const addNews = () => {
    setEditMode(true);
  };

  const updateNews = newsToUpdate => {
    setNews(newsToUpdate);
    setEditMode(true);
  };
 
  const handleSubmit = e => {
    e.preventDefault();
    setEditMode(false);
  };

  return (
    <div className="admin-news">
      <div>
        <ListRouteComponents />
      </div>
      <div className="admin-news-content">
        <h1>News Index</h1>

        {editMode ? (
          <NewsForm updateOneNews={news} action={e => handleSubmit(e)} />
        ) : (
          <div>
            <NewsList
              addNews={() => addNews()}
              updateNews={newsToUpdate => updateNews(newsToUpdate)}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default News;
