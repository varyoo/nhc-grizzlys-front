import {
  get20news,
  getNewsByTitle,
  deleteNews,
} from "controler/apiService/apiService.News";
import React, {useEffect, useState} from "react";
import {ButtonForms} from "utils/inputs/inputs";
import {InputsText} from "utils/inputs/inputs";

const NewsList = ({addNews, updateNews}) => {
  const [allNews, setAllNews] = useState([]);
  const [indexNews, setIndexNews] = useState(0);
  const [isLast, setIsLast] = useState(true);
  const [searchNews, setSearchNews] = useState("");

  useEffect(() => {
    get20news(indexNews).then(e => {
      setAllNews(e.news);
      setIsLast(e.newsLeft);
    });
  }, [indexNews, addNews]);

  const handleSearchNews = (type, value) => {
    setSearchNews(value);
    if (searchNews.length >= 2) {
      getNewsByTitle(searchNews).then(res => setAllNews(res));
    }
  };
  
  const deleteNewsById = (e, id) => {
    deleteNews(id).then(res => console.log(res));
  };

  return (
    <div>
      <InputsText
        label="Rechercher une news ?"
        defaultValue=""
        type="rechercher"
        action={(type, e) => handleSearchNews(type, e)}
      />
      <div>
        <ButtonForms label="Ajouter une news" action={e => addNews()} />
      </div>
      <div>
        {allNews &&
          allNews.map((value, key) => {
            return (
              <div key={key}>
                <span>{value.title_news}</span>
                <span>{value.date_news}</span>
                <ButtonForms label={"editer"} action={e => updateNews(value)} />
                <ButtonForms
                  label={"delete"}
                  action={e => deleteNewsById(e, value.id_news)}
                />
              </div>
            );
          })}
        {indexNews !== 0 && (
          <button onClick={() => setIndexNews(indexNews - 10)}>Precedent</button>
        )}
        {isLast && <button onClick={() => setIndexNews(indexNews + 10)}>Suivant</button>}
      </div>
    </div>
  );
};

export default NewsList;
