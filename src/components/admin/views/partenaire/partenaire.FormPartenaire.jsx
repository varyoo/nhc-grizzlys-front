import React, {useState, useEffect} from "react";
import {Form} from "react-bootstrap";
import {ButtonForms, InputsText, InputsFile} from "utils/inputs/inputs";
import Wysiwyg from "utils/inputs/wysiwyg";
import {
  ControllerStateUpdatePartenaire,
  defaultStatePartenaire,
} from "controler/control.data.parteniare";
import {
  addPartenaires,
  updatePartenaires,
} from "controler/apiService/apiService.Partenaires";

const FormPartenaire = ({updateToPartenaire}) => {
  console.log(updateToPartenaire);
  const [partenaire, setPartenaire] = useState(defaultStatePartenaire);
  const [picture, setPicture] = useState({
    name_picture: "",
    alt_picture: "",
  });

  useEffect(() => {
    if (updateToPartenaire.id_partner)
      ControllerStateUpdatePartenaire(updateToPartenaire, (partenaire, picture) => {
        setPartenaire(partenaire);
        setPicture(picture);
      });
  }, [updateToPartenaire]);

  const [file, setFile] = useState([]);

  const handleChangePartenaire = (type, value) => {
    setPartenaire({...partenaire, [type]: value});
  };
  const handleChangePicture = (type, value) => {
    setPicture({...picture, [type]: value});
  };

  const handleChangeFile = file => {
    setFile(file);
  };

  const handleSubmit = e => {
    let sub = {...partenaire, picture: picture};
    const forms = new FormData();
    forms.append("partenaire", JSON.stringify(sub));
    forms.append("files", file[0]);
    if (updateToPartenaire.id_partner) {
      updatePartenaires(updateToPartenaire.id_partner, forms).then(res =>
        console.log(res)
      );
    } else {
      addPartenaires(forms).then(res => console.log(res));
    }
  };

  return (
    <Form style={{width: "100%", margin: "0 auto"}}>
      <InputsText
        label="Nom partenaire"
        defaultValue={partenaire.name_partner}
        type="name_partner"
        action={(type, e) => handleChangePartenaire(type, e)}
      />
      <Wysiwyg
        label="Description partneraire"
        defaultValue={partenaire.description_partner}
        type="description_partner"
        action={(type, html) => handleChangePartenaire(type, html)}
      />
      <InputsText
        label="Mail partenaire"
        defaultValue={partenaire.mail_partner}
        type="mail_partner"
        action={(type, e) => handleChangePartenaire(type, e)}
      />
      <InputsText
        label="Phone partenaire"
        defaultValue={partenaire.phone_partner}
        type="phone_partner"
        action={(type, e) => handleChangePartenaire(type, e)}
      />
      <InputsText
        label="Rue partenaire"
        defaultValue={partenaire.street_partner}
        type="street_partner"
        action={(type, e) => handleChangePartenaire(type, e)}
      />
      <InputsText
        label="Ville partenaire"
        defaultValue={partenaire.city_partner}
        type="city_partner"
        action={(type, e) => handleChangePartenaire(type, e)}
      />
      <InputsText
        label="Code postal partenaire"
        defaultValue={partenaire.zip_partner}
        type="zip_partner"
        action={(type, e) => handleChangePartenaire(type, e)}
      />
      <InputsText
        label="Site partenaire"
        defaultValue={partenaire.website_partner}
        type="website_partner"
        action={(type, e) => handleChangePartenaire(type, e)}
      />
      <InputsFile label="Images partenaire" action={e => handleChangeFile(e)} />
      <InputsText
        label="Nom images"
        defaultValue={picture.name_picture}
        type="name_picture"
        action={(type, e) => handleChangePicture(type, e)}
      />
      <InputsText
        label="Discription de l'images"
        type="alt_picture"
        defaultValue={picture.alt_picture}
        action={(type, e) => handleChangePicture(type, e)}
      />
      <ButtonForms label="Ajoutez l'article" action={e => handleSubmit(e)} />
    </Form>
  );
};

export default FormPartenaire;
