import React, {useEffect, useState} from "react";
import {ButtonForms} from "utils/inputs/inputs";
import {getAllPartenaires} from "controler/apiService/apiService.Partenaires";

const ListPartenaires = ({addPartenaire, deletePartenaire, updatePartenaire}) => {
  const [allPartenaires, setAllPartenaires] = useState([]);

  useEffect(() => {
    getAllPartenaires().then(res => setAllPartenaires(res));
  }, [addPartenaire]);

  return (
    <div>
      <div>
        <ButtonForms label="Ajouter un partenaire" action={e => addPartenaire()} />
      </div>
      <div>
        <h1>Liste des partenaires</h1>
        {allPartenaires &&
          allPartenaires.map((value, key) => {
            return (
              <div key={key}>
                <span>{value.name_partner}</span>
                <ButtonForms label={"editer"} action={e => updatePartenaire(value)} />
                <ButtonForms
                  label={"delete"}
                  action={e => deletePartenaire(e, value.id_partner)}
                />
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default ListPartenaires;
