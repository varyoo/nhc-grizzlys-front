import React, {useState} from "react";
import "styles/admin.styles/admin.partenaire.styles.scss";
import ListRouteComponents from "components/admin/ListRouteComponents";
import FormPartenaire from "./partenaire.FormPartenaire";
import ListPartenaire from "./partenaire.ListPartenaire";
import {deletePartenaires} from "controler/apiService/apiService.Partenaires";

const Partenaire = () => {
  const [editMode, setEditMode] = useState(false);
  const [partenaire, setPartenaire] = useState({});

  const addPartenaire = () => {
    setEditMode(true);
  };

  const deletePartenaire = (e, id) => {
    e.preventDefault();
    deletePartenaires(id);
  };

  const updatePartenaire = PartenaireToUpdate => {
    setPartenaire(PartenaireToUpdate);
    setEditMode(true);
  };

  const handleSubmit = e => {
    e.preventDefault();
    setEditMode(false);
  };
  return (
    <div className="admin-partenaire">
      <div>
        <ListRouteComponents />
      </div>
      <div className="admin-partenaire-content">
        <div className="admin-news-content">
          <h1>PARTENAIRE</h1>
          {editMode ? (
            <FormPartenaire
              updateToPartenaire={partenaire}
              action={e => handleSubmit(e)}
            />
          ) : (
            <ListPartenaire
              addPartenaire={() => addPartenaire()}
              deletePartenaire={(e, id) => deletePartenaire(e, id)}
              updatePartenaire={partenaireToUpdate =>
                updatePartenaire(partenaireToUpdate)
              }
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default Partenaire;
