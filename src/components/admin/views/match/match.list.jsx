import React from "react";

const MatchList = ({type, data, sortMatch}) => {
  return (
    <>
      <h1 style={{margin: " 0 20px "}} onClick={e => sortMatch(e, type)}>
        {type}
      </h1>
      <ul style={{margin: " 0 20px "}}>
        {data.map((value, key) => {
          if (type === "date") {
            return (
              <div key={key}>
                <li>{value.date_match}</li>
              </div>
            );
          }
          if (type === "match") {
            return (
              <div key={key}>
                <li>{value.title}</li>
              </div>
            );
          }
          if (type === "equipe") {
            return (
              <div key={key}>
                <li>{value.team.name_team}</li>
              </div>
            );
          }
          if (type === "lieu") {
            return (
              <div key={key}>
                <li>{value.location}</li>
              </div>
            );
          }
        })}
      </ul>
    </>
  );
};

export default MatchList;
