import React, {useEffect, useState} from "react";
import {RadioBox} from "../../../../utils/inputs/inputs";
import {getAllTeams} from "../../../../controler/apiService/apiService.Teams";
import {addMatch} from "../../../../controler/apiService/apiService.match";

const AddMatch = () => {
  const [team, setTeam] = useState([]);

  useEffect(() => {
    getAllTeams()
      .then(res => setTeam(res))
      .catch(err => console.log(err));
  }, []);

  const [state, setState] = useState({
    date_match: "",
    hour_match: "",
    location: "",
    title: "",
    team: {},
  });

  const checkBoxHandler = value => {
    setState({...state, team: value});
  };

  const convertDate = value => {
    let temp = value.split("/").join("-");
    setState({...state, date_match: temp});
  };

  const convertTime = value => {
    let temp = value.split(":").join("");
    setState({...state, hour_match: temp});
  };
  const handleSubmit = () => {
    const forms = new FormData();
    forms.append("match", JSON.stringify(state));
    addMatch(forms);
  };

  return (
    <div>
      <form
        onSubmit={e => {
          e.preventDefault();
          handleSubmit();
        }}
      >
        <label htmlFor="date_match">Date : </label>
        <input
          type="date"
          id="date_match"
          name="date_match"
          onChange={e => convertDate(e.target.value)}
        />
        <label htmlFor="hour_match">Heur : </label>
        <input
          type="time"
          id="hour_match"
          name="hour_match"
          onChange={e => convertTime(e.target.value)}
        />
        <label htmlFor="location">Lieu : </label>
        <input
          type="text"
          id="location"
          name="location"
          onChange={e => setState({...state, location: e.target.value})}
        />
        <label htmlFor="title">Match : </label>
        <input
          type="text"
          id="title"
          name="title"
          onChange={e => setState({...state, title: e.target.value})}
        />
        {team.length !== undefined
          ? team.map((value, key) => {
              return (
                <RadioBox
                  key={key}
                  label={value.name_team}
                  value={value.id_team_team}
                  action={e => checkBoxHandler(value)}
                />
              );
            })
          : null}
        <button type="submit">Ajouter</button>
      </form>
    </div>
  );
};

export default AddMatch;
