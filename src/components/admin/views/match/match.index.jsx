import React, {useEffect, useState} from "react";
import MatchList from "./match.list";
import {get20Match} from "../../../../controler/apiService/apiService.match";
import ListRouteComponents from "components/admin/ListRouteComponents";
import AddMatch from "./match.addMatch";

const Match = () => {
  const tri = ["date", "match", "equipe", "lieu", "action"];
  const [nextMatch, setNextMatch] = useState(false);
  const [index, setIndex] = useState(0);
  const [sorted, setSorted] = useState();
  const [bool, setBool] = useState(false);
  const [addMatch, setAddMatch] = useState(false);

  useEffect(() => {
    get20Match(index)
      .then(res => {
        setSorted(res.matchEntities);
        setNextMatch(res.matchLeft);
      })
      .catch(err => console.log(err));
  }, [index]);

  const sortMatch = (e, type) => {
    let temp = [...sorted];
    if (type === "date") {
      if (bool) {
        setSorted(
          temp
            .sort(function(a, b) {
              var aComps = a.date_match;
              var bComps = b.date_match;
              return aComps - bComps;
            })
            .reverse()
        );
      }
      if (bool === false) {
        setSorted(
          temp
            .sort(function(a, b) {
              var aComps = a.date_match;
              var bComps = b.date_match;
              return aComps - bComps;
            })
            .reverse()
        );
      }
      setBool(!bool);
    }
    if (type === "match") {
      if (bool) {
        setSorted(
          temp
            .sort(function(a, b) {
              if (a.titre < b.titre) return -1;
              if (a.titre > b.titre) return 1;
              return 0;
            })
            .reverse()
        );
      } else {
        setSorted(
          temp
            .sort(function(a, b) {
              if (a.titre < b.titre) return -1;
              if (a.titre > b.titre) return 1;
              return 0;
            })
            .reverse()
        );
      }
    }
    setBool(!bool);
  };

  return (
    <>
      {addMatch ? (
        <AddMatch />
      ) : (
        <div>
          {sorted !== undefined ? (
            <div>
              <ListRouteComponents />
              {tri.map((value, key) => {
                return (
                  <div key={key}>
                    <MatchList
                      type={value}
                      data={sorted}
                      sortMatch={(e, type) => sortMatch(e, type)}
                    />
                  </div>
                );
              })}
            </div>
          ) : null}
          <div>
            {index !== 0 && <h1 onClick={e => setIndex(-20)}>precedant</h1>}
            {nextMatch && <h1 onClick={e => setIndex(+20)}>suivant</h1>}
          </div>
          <div>
            <button onClick={e => setAddMatch(true)}>Ajouter un match</button>
          </div>
        </div>
      )}
    </>
  );
};

export default Match;
