import ListRouteComponents from "components/admin/ListRouteComponents";
import {deleteTraining} from "controler/apiService/apiService.Entrainements";
import React, {useState} from "react";
import TrainingForm from "./form.entrainement";
import TrainingList from "./liste.entrainement";

const Entrainement = () => {
  const [edit, setEdit] = useState(false);
  const [training, setTraining] = useState();

  const addTraining = () => {
    setEdit(true);
  };

  const delTraining = trainingToDelete => {
    deleteTraining(trainingToDelete.id_training);
    alert(
      `l'entrainement du  ${trainingToDelete.day_training} à ${trainingToDelete.start_hour_training} a été suprimé`
    );
  };

  const updateTraining = trainingToUpdate => {
    setTraining(trainingToUpdate);
    setEdit(true);
  };

  const handleSubmit = e => {
    e.preventDefault();
    setEdit(false);
  };

  return (
    <div>
      <h1>Entrainements Index</h1>
      <div>
        <ListRouteComponents />
      </div>
      {edit ? (
        <TrainingForm updateTraining={training} action={e => handleSubmit(e)} />
      ) : (
        <TrainingList
          addTraining={() => addTraining()}
          updateTraining={trainingToUpdate => updateTraining(trainingToUpdate)}
          delTraining={trainingToDelete => delTraining(trainingToDelete)}
        />
      )}
    </div>
  );
};
export default Entrainement;
