import React, {useEffect, useState} from "react";
import "styles/users.styles/users.styles.entrainement.scss";
import {postTraining, putTraining} from "controler/apiService/apiService.Entrainements";
import CheckboxEquipes from "utils/inputs/checkboxEquipes";
import {RadioBox, CheckBox, InputsTime} from "utils/inputs/inputs";
import {
  ControllerStateUpdateTraining,
  defaultStateTraining,
} from "controler/controler.data.training";

const TrainingForm = ({updateTraining}) => {
  const [training, setTraining] = useState(defaultStateTraining);
  const [vestTeam, setVestTeam] = useState([]);

  useEffect(() => {
    //gestion vestiaire
    if (vestTeam.includes("A") && vestTeam.includes("B")) {
      setTraining({...training, cloakroom_training: "Vestiaires A et B"});
    } else if (vestTeam.includes("A")) {
      setTraining({...training, cloakroom_training: "Vestiaire A"});
    } else if (vestTeam.includes("B")) {
      setTraining({...training, cloakroom_training: "Vestiaire B"});
    } else {
      setTraining({...training, cloakroom_training: ""});
    }
    //gestion update/add
    if (updateTraining) {
      ControllerStateUpdateTraining(updateTraining, training => {
        setTraining(training);
      });
    }
  }, [vestTeam]);
  //gestion affichage vestiaires
  const vest = vestiaire => {
    let e = [...vestTeam];
    if (e.includes(vestiaire)) {
      e.splice(e.indexOf(vestiaire), 1);
    } else {
      e.push(vestiaire);
    }
    setVestTeam(e);
  };

  //gestion radiobox
  const onRadioPeriodclick = e => {
    setTraining({...training, period: e});
  };

  const onRadioDayclick = e => {
    setTraining({...training, day_training: e});
  };

  const callB = value => {
    setTraining({...training, teams: value});
  };

  const handleSubmit = () => {
    //gestion periode
    if (training.period === "") {
      alert("veuillez selectionner une période");
    } else {
      if (updateTraining) {
        putTraining(updateTraining.id_training, training);
      } else {
        postTraining(training);
      }
    }
  };

  const onTimeChangeHandler = (type, value) => {
    let time = value.hour + value.minute;
    switch (type) {
      case "start":
        setTraining({...training, start_hour_training: time});
        break;
      case "end":
        setTraining({...training, end_hour_training: time});
        break;
      default:
        console.log("default switch");
        break;
    }
  };

  return (
    <>
      <div>
        Période :
        <RadioBox
          label="Période scolaire"
          value="scolaire"
          id="scolaire"
          name="periode"
          isChecked={training.period.includes("scolaire") && true}
          action={e => onRadioPeriodclick(e)}
        />
        <RadioBox
          label="Vacances"
          value="vacances"
          id="vacances"
          name="periode"
          isChecked={training.period.includes("vacances") && true}
          action={e => onRadioPeriodclick(e)}
        />
      </div>
      <div>
        Jour :
        <RadioBox
          label="Lundi"
          value="lundi"
          id="lundi"
          name="day"
          isChecked={training.day_training.includes("lundi") && true}
          action={e => onRadioDayclick(e)}
        />
        <RadioBox
          label="Mardi"
          value="mardi"
          id="mardi"
          name="day"
          isChecked={training.day_training.includes("mardi") && true}
          action={e => onRadioDayclick(e)}
        />
        <RadioBox
          label="Mercredi"
          value="mercredi"
          id="mercredi"
          name="day"
          isChecked={training.day_training.includes("mercredi") && true}
          action={e => onRadioDayclick(e)}
        />
        <RadioBox
          label="Jeudi"
          value="jeudi"
          id="jeudi"
          name="day"
          isChecked={training.day_training.includes("jeudi") && true}
          action={e => onRadioDayclick(e)}
        />
        <RadioBox
          label="Vendredi"
          value="vendredi"
          id="vendredi"
          name="day"
          isChecked={training.day_training.includes("vendredi") && true}
          action={e => onRadioDayclick(e)}
        />
        <RadioBox
          label="Samedi"
          value="samedi"
          id="samedi"
          name="day"
          isChecked={training.day_training.includes("samedi") && true}
          action={e => onRadioDayclick(e)}
        />
        <RadioBox
          label="Dimanche"
          value="dimanche"
          id="dimanche"
          name="day"
          isChecked={training.day_training.includes("dimanche") && true}
          action={e => onRadioDayclick(e)}
        />
      </div>
      <InputsTime
        defaultValue={training.start_hour_training}
        label="Heure de début :"
        type="start"
        action={(type, value) => onTimeChangeHandler(type, value)}
      />
      <InputsTime
        defaultValue={training.end_hour_training}
        label="Heure de fin :"
        type="end"
        action={(type, value) => onTimeChangeHandler(type, value)}
      />
      <div>
        Vestiaire(s) :
        <CheckBox
          label="A"
          value="A"
          id="chekA"
          isChecked={training.cloakroom_training.includes("A") && true}
          action={e => vest(e)}
        />
        <CheckBox
          label="B"
          value="B"
          id="chekB"
          isChecked={training.cloakroom_training.includes("B") && true}
          action={e => vest(e)}
        />
      </div>
      <div>
        Equipe(s) :
        <span>
          <div>
            <CheckboxEquipes
              teamSelected={training.teams}
              returnValue={value => callB(value)}
            />
          </div>
        </span>
      </div>
      <input type="submit" value="Valider" onClick={() => handleSubmit()} />
    </>
  );
};

export default TrainingForm;
