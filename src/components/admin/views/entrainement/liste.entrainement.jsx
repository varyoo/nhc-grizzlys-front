import React, {useEffect, useState} from "react";
import {ButtonForms} from "utils/inputs/inputs";
import {getAllTrainings} from "controler/apiService/apiService.Entrainements";

const TrainingList = ({addTraining, updateTraining, delTraining}) => {
  const [allTrainings, setAllTrainings] = useState([]);

  useEffect(() => {
    getAllTrainings().then(res => {
      setAllTrainings(res);
    });
  }, []);
  const deleteTraining = value => {
    delTraining(value);
    getAllTrainings().then(res => {
      setAllTrainings(res);
    });
  };

  return (
    <div>
      <ButtonForms label="nouvel Entrainement" action={e => addTraining()} />
      <h1>Liste des entrainement :</h1>
      {allTrainings &&
        allTrainings.map((value, key) => {
          return (
            <div key={key}>
              Entrainement du {value.day_training} à {value.start_hour_training} (
              {value.period})
              <ButtonForms label="éditer" action={e => updateTraining(value)} />
              <ButtonForms label="X" action={e => deleteTraining(value)} />
            </div>
          );
        })}
    </div>
  );
};
export default TrainingList;
