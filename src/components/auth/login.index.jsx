import React, {useState, useEffect} from "react";
import {Redirect} from "react-router-dom";
import {postLogin} from "controler/apiService/apiService.Login";

export default function Login(props) {
  const {from} = props.location.state || {from: {pathname: "/"}};

  const [redirectToReferrer, setRedirectToReferrer] = useState(false);

  const [state, setState] = useState({
    username: "",
    password: "",
  });

  useEffect(() => {
    if (localStorage.getItem("token")) {
      setRedirectToReferrer(true);
    } else {
      setRedirectToReferrer(false);
    }
  }, []);

  const handleChange = (type, value) => {
    switch (type) {
      case "username":
        setState({...state, username: value});
        break;
      case "password":
        setState({...state, password: value});
        break;
      default:
        console.log("err");
        break;
    }
  };

  const login = () => {
    postLogin(state).then(e => {
      localStorage.setItem("token", e);
      setRedirectToReferrer(true);
    });
  };

  if (redirectToReferrer) {
    return <Redirect to={from} />;
  }

  return (
    <div>
      <input
        type="text"
        value={state.username}
        onChange={e => handleChange("username", e.target.value)}
      />
      <input
        type="password"
        value={state.password}
        onChange={e => handleChange("password", e.target.value)}
      />
      <button onClick={() => login()}>Connexion</button>
    </div>
  );
}
